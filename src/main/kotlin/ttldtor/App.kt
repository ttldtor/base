package ttldtor

import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.layout.VBox
import javafx.stage.Stage

class App : Application() {
    override fun start(primaryStage: Stage?) {
        val layout = VBox().apply {
            children.add(Label("Base"))
        }

        primaryStage?.run {
            scene = Scene(layout)
            show()
        }
    }
}

fun main() {
    println("123")
    Application.launch(App::class.java)
}